package com.example.lesson7;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void showdialog(View view)
	{
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Dialog1 df = new Dialog1();
        df.show(ft, "");

	}
	
	public void showinputdialog(View view)
	{
		Dialog2 dialog = new Dialog2();
		dialog.show(getSupportFragmentManager(),"Dialog");

	}
	
	public void onUserSelectValue(String selectedValue)
	{
		TextView textview = (TextView) findViewById(R.id.textView);
		textview.setText(selectedValue);
	}
	

}
