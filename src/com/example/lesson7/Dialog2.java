package com.example.lesson7;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.os.Bundle;
import android.widget.EditText;

public class Dialog2 extends DialogFragment implements OnClickListener{
	
		EditText edittext;
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {

	    	   edittext = new EditText(getActivity());
		       edittext.setInputType(InputType.TYPE_CLASS_TEXT);
		        return new AlertDialog.Builder(getActivity()).setTitle("Enter text").setMessage("Please enter text").setPositiveButton("OK",this).setNegativeButton("Cancel",null).setView(edittext).create();
	    }

		@Override
		public void onClick(DialogInterface arg0, int arg1) {
			String val = edittext.getText().toString();
			MainActivity m  = (MainActivity) getActivity();
			m.onUserSelectValue(val);
			arg0.dismiss();
			
		}


}
