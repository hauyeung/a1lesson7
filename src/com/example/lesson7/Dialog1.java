package com.example.lesson7;


import android.support.v4.app.DialogFragment;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class Dialog1 extends DialogFragment implements OnClickListener{
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

	      return new AlertDialog.Builder(getActivity()).setTitle("Message").setMessage("Message").setPositiveButton("OK", this).create();
    }
    
	@Override
	public void onClick(DialogInterface arg0, int arg1) {
    	dismiss();
    }

}
